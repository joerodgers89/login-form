## Using Bandit to find problems in codebase

### Original output from Bandit:

```
[main]  INFO    profile include tests: None
[main]  INFO    profile exclude tests: None
[main]  INFO    cli include tests: None
[main]  INFO    cli exclude tests: None
[main]  INFO    running on Python 3.11.2
Run started:2024-02-20 17:53:26.263989

Test results:
>> Issue: [B106:hardcoded_password_funcarg] Possible hardcoded password: 'super_secret_key'
   Severity: Low   Confidence: Medium
   CWE: CWE-259 (https://cwe.mitre.org/data/definitions/259.html)
   More Info: https://bandit.readthedocs.io/en/1.7.5/plugins/b106_hardcoded_password_funcarg.html
   Location: ./login_form/__init__.py:7:4
6           app = Flask(__name__, instance_relative_config=True)
7           app.config.from_mapping(
8               SECRET_KEY='super_secret_key',
9               DATABASE=os.path.join(app.instance_path, 'login_form.sqlite'),
10          )
11

--------------------------------------------------
>> Issue: [B608:hardcoded_sql_expressions] Possible SQL injection vector through string-based query construction.
   Severity: Medium   Confidence: Medium
   CWE: CWE-89 (https://cwe.mitre.org/data/definitions/89.html)
   More Info: https://bandit.readthedocs.io/en/1.7.5/plugins/b608_hardcoded_sql_expressions.html
   Location: ./login_form/user.py:10:6
9           db.execute(
10            "INSERT INTO user (username, password) VALUES ('"+username+"', '"+password+"')",
11            ()

--------------------------------------------------
>> Issue: [B608:hardcoded_sql_expressions] Possible SQL injection vector through string-based query construction.
   Severity: Medium   Confidence: Medium
   CWE: CWE-89 (https://cwe.mitre.org/data/definitions/89.html)
   More Info: https://bandit.readthedocs.io/en/1.7.5/plugins/b608_hardcoded_sql_expressions.html
   Location: ./login_form/user.py:19:6
18          user = db.execute(
19            "SELECT id, username, password FROM user WHERE username = '" + username + "' AND password = '" + password + "'"
20          ).fetchone()

--------------------------------------------------

Code scanned:
        Total lines of code: 198
        Total lines skipped (#nosec): 0

Run metrics:
        Total issues (by severity):
                Undefined: 0
                Low: 1
                Medium: 2
                High: 0
        Total issues (by confidence):
                Undefined: 0
                Low: 0
                Medium: 3
                High: 0
Files skipped (0):
```

### Output from bandit after changes to codebase: 

```
[main]  INFO    profile include tests: None
[main]  INFO    profile exclude tests: None
[main]  INFO    cli include tests: None
[main]  INFO    cli exclude tests: None
[main]  INFO    running on Python 3.11.2
Run started:2024-02-21 13:38:18.509424

Test results:
        No issues identified.

Code scanned:
        Total lines of code: 199
        Total lines skipped (#nosec): 0

Run metrics:
        Total issues (by severity):
                Undefined: 0
                Low: 0
                Medium: 0
                High: 0
        Total issues (by confidence):
                Undefined: 0
                Low: 0
                Medium: 0
                High: 0
Files skipped (0):
```

## Vulnerabilities Found

* Hardcoded secret key used for Flask session set to 'super_secret_key'. Changed this to a 16 digit hex hash using the secrets library `SECRET_KEY=secrets.token_hex(16)`.
* String based SQL query construction which can lead the app to be vulnerable to SQL injection. Changed theses lines of code in user.py to use the `?` string bindings to parameterise the queries to help prevent SQL injection.
* Passwords stored in database in plain text which is less secure if database info is leaked. I changed the codebase to store the hashed password in the database instead using `werkzeug.security.generate_password_hash()` and then checked them during a login attempt using `werkzeug.security.heck_password_hash()`.
* No checking of password when registering an account which can lead to insecure and easy to guess password. Changed code to error check against an empty password and it make sure password is 8 charaters or more. Of course more checking could be included to make sure it contains a special character, uppercase letter, and/or number.

## Other info

* Through testing XSS doesn't seem possible so no escaping is necessary. This is because the only time user entered info is put onto the page is in index.html "You are now logged in as...". This is input via jinja templates which automatically escapse html.
* Debug mode is not enabled on the app meaning users can't see code snippets or enter the console during an error.